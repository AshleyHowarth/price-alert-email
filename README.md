# Apple Airpod Price Alert Email
An automated daily price email alert about Apple Airpods. The email will reflect the today’s price and how it compares to the average price from previous days.

## Description
Specifically, this project contains 2 files. The first file (prepping the db final.py) initiates the Apple Airpod SQlite database by creating the tables and adding the various airpod products to the product table. The second file (daily auto.py) scrapes the live price of each airpod product at the time the code is run, add that data to the prices table in the database and sends an email to an explicit recipient outlining the current price and how that price compares to the average price of the last seven days. The second file in question should be set up to run automatically onces every morning on a machine of choice. Alternatively, the python code provided in the third file (daily auto within code automation.py) will automatically run everyday, however the python shell must be in a perpetual while loop on a machine, which would likely be a waste of recourses. 

Given more time, I'd have run this code over a series of days/weeks in order to build real data in the db and send true email updates. However, I have had to enrich the data manually to show the code works, so the price fluctuations in the db are inaccurate.

The database has a normalised structure, meaning it is split into two tables: Products (ID and Name) and Prices over time (ID, Date, Price). 

To run this code successfully, please execute the following in order:
1. prepping the db final.py
2. data enrichment.py
3. daily auto.py

This will send you an email for todays information. To automate, either run 'daily auto within code automation.py' (and ensure the date and time matches the starting date and time) or setup a scheduled task in the Task Scheduler to run the 'daily auto.py' script everyday.

The email will be sent from an email I created, testerforcreditsuisse@gmail.com, and recieved by the email input into the email address section of the code (line 17). this new email was created to ensure Python could access it and send emails from it without security concerns. 

The various Apple Airpods products were chosen to illustrate how I was manage a database of many products.