# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 13:05:37 2021

@author: AshleyHowarth
"""

########must need this code to run daily

import datetime
import requests
from bs4 import BeautifulSoup
import re
import sqlite3

#email receiver
receiver_email = 'PLEASE ENTER EMAIL ADDRESS TO RECIEVE UPDATES HERE WITHIN THESE QUOTES'
#get todays date
todaydt = datetime.datetime.now()
today = str(todaydt.strftime('%Y-%m-%d %H:%M:%S')[:10])
#scrape the htmls for the products
airpods3 = r'https://www.apple.com/uk/shop/product/MME73ZM/A/airpods-3rd-generation'
airpodsmax = r'https://www.apple.com/uk/shop/buy-airpods/airpods-max'
airpodspro = r'https://www.apple.com/uk/shop/product/MLWK3ZM/A/airpods-pro'
A3 = requests.get(airpods3)
AM = requests.get(airpodsmax)
AP = requests.get(airpodspro)


#apply beautiful soup
A3soup = BeautifulSoup(A3.text,features="lxml")
AMsoup = BeautifulSoup(AM.text,features="lxml")
APsoup = BeautifulSoup(AP.text,features="lxml")

#Airpods 3rd Gen price extraction using find function in the soup and regular expressions to remove £

tag = 'div' 
attributes = {'class':'rf-pdp-currentprice'} 
A3price_soup = A3soup.find(tag, attributes) 
A3_Price_today = float(re.compile('[^£]+').findall(A3price_soup.text)[0])

#Aipods Pro
APprice_soup = APsoup.find(tag, attributes)
AP_Price_today = float(re.compile('[^£]+').findall(APprice_soup.text)[0])

#Airpods Max have various colours and a slightly diferent HTML structure
AMattributes = {'class':'item equalize-capacity-button-height'} 

AMprice_soup = AMsoup.find_all(tag, AMattributes)
AM_prices_dict = {}
for i in AMprice_soup:
    colour = i.find('span', {'class':'dimensionColor'}).text
    price = i.find('span', {'class':'current_price'}).text
    price = float(re.compile('[^£]+').findall(price)[0])
    AM_prices_dict[colour] = price

#add to the sqlite db with todays data
print(today)
con = sqlite3.connect('Airpods.db')
cur = con.cursor()


cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "{date1}", {AP_Price_today})'.format(date1=today, AP_Price_today = AP_Price_today))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "{date1}", {A3_Price_today})'.format(date1=today, A3_Price_today = A3_Price_today))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "{date1}", {AM_prices_dict})'.format(date1=today, AM_prices_dict = AM_prices_dict['Silver']))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "{date1}", {AM_prices_dict})'.format(date1=today, AM_prices_dict = AM_prices_dict['Space Grey']))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "{date1}", {AM_prices_dict})'.format(date1=today, AM_prices_dict = AM_prices_dict['Sky Blue']))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "{date1}", {AM_prices_dict})'.format(date1=today, AM_prices_dict = AM_prices_dict['Green']))
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "{date1}", {AM_prices_dict})'.format(date1=today, AM_prices_dict = AM_prices_dict['Pink']))




#GETTING SOME SUMMARY DATA
import statistics
#get airpod pro prices
cur.execute("select * from Apple_Prices_Daily where ProductID=1")
APlist = (cur.fetchall())
APlist[0][:2]
AP_price_list = []
for i in APlist:
    AP_price_list.append(i[2])


AP_avg_price_last_7_days = statistics.mean(AP_price_list[:7])
AP_avg_price_last_7_days
#airpods 3rd gen
cur.execute("select * from Apple_Prices_Daily where ProductID=2")
A3list = (cur.fetchall())
A3list[0][:2]
A3_price_list = []
for i in A3list:
    A3_price_list.append(i[2])


A3_avg_price_last_7_days = statistics.mean(A3_price_list[:7])
A3_avg_price_last_7_days
#SILVER airpod max
cur.execute("select * from Apple_Prices_Daily where ProductID=3")
AMSlist = (cur.fetchall())
AMSlist[0][:2]
AMS_price_list = []
for i in AMSlist:
    AMS_price_list.append(i[2])


AMS_avg_price_last_7_days = statistics.mean(AMS_price_list[:7])
AMS_avg_price_last_7_days
#SPACE GREY
cur.execute("select * from Apple_Prices_Daily where ProductID=4")
AMGlist = (cur.fetchall())
AMGlist[0][:2]
AMG_price_list = []
for i in AMGlist:
    AMG_price_list.append(i[2])


AMG_avg_price_last_7_days = statistics.mean(AMG_price_list[:7])
AMG_avg_price_last_7_days
#SKY BLUE
cur.execute("select * from Apple_Prices_Daily where ProductID=5")
AMBlist = (cur.fetchall())
AMBlist[0][:2]
AMB_price_list = []
for i in AMBlist:
    AMB_price_list.append(i[2])


AMB_avg_price_last_7_days = statistics.mean(AMB_price_list[:7])
AMB_avg_price_last_7_days
#GREEN
cur.execute("select * from Apple_Prices_Daily where ProductID=6")
AMGlist = (cur.fetchall())
AMGlist[0][:2]
AMG_price_list = []
for i in AMGlist:
    AMG_price_list.append(i[2])


AMG_avg_price_last_7_days = statistics.mean(AMG_price_list[:7])
AMG_avg_price_last_7_days
#PINK
cur.execute("select * from Apple_Prices_Daily where ProductID=7")
AMPlist = (cur.fetchall())
AMPlist[0][:2]
AMP_price_list = []
for i in AMPlist:
    AMP_price_list.append(i[2])


AMP_avg_price_last_7_days = statistics.mean(AMP_price_list[:7])
AMP_avg_price_last_7_days

con.commit()

con.close()

#sending the email summarising the current data in the db for each product


import smtplib, ssl

port = 465
password = 'testpassword01-'
print(password)
context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
    server.login("testerforcreditsuisse@gmail.com", password)
    subject = 'Apple Airpods Price Updates: Daily'
    
    body = f'''Good Morning, \n\n
    Todays date is {todaydt.day}/{todaydt.month}/{todaydt.year}\n
    The current price of Apple Airpods are:\n
    Airpods 3rd Gen: {A3_Price_today}0 GBP 
    This compares to the average price over the last seven days: {int(A3_avg_price_last_7_days)}.00 GBP\n
    Airpods Pro: {AP_Price_today}0 GBP 
    This compares to the average price over the last seven days: {int(AP_avg_price_last_7_days)}.00 GBP\n
    Airpods Max (Silver): {AM_prices_dict["Silver"]}0 GBP 
    This compares to the average price over the last seven days: {int(AMS_avg_price_last_7_days)}.00 GBP\n
    Airpods Max (Space Grey): {AM_prices_dict["Space Grey"]}0 GBP 
    This compares to the average price over the last seven days: {int(AMG_avg_price_last_7_days)}.00 GBP\n
    Airpods Max (Sky Blue): {AM_prices_dict["Sky Blue"]}0 GBP 
    This compares to the average price over the last seven days: {int(AMB_avg_price_last_7_days)}.00 GBP\n
    Airpods Max (Green): {AM_prices_dict["Green"]}0 GBP 
    This compares to the average price over the last seven days: {int(AMG_avg_price_last_7_days)}.00 GBP\n
    Airpods Max (Pink): {AM_prices_dict["Pink"]}0 GBP 
    This compares to the average price over the last seven days: {int(AMP_avg_price_last_7_days)}.00 GBP'''
    
    
    sender_email = "testerforcreditsuisse@gmail.com"
    msg = f'Subject: {subject}\n\n{body}'
    server.sendmail(sender_email, receiver_email, msg)





