# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 13:19:02 2021

@author: AshleyHowarth
"""


#intiate the sqlite db
import sqlite3
con = sqlite3.connect('Airpods.db')
cur = con.cursor()

# Create tables

prods = '''
CREATE TABLE Apple_Products (ProductID int identity, Product_name varchar(50), 
constraint PD_pk primary key (ProductID))
'''

prices = '''
CREATE TABLE Apple_Prices_Daily (ProductID int, Price_Date date, Price money, 
constraint PDY_pk primary key (ProductID, Price_Date), 
constraint FK foreign key (ProductID) references Apple_Products (ProductID))
'''

cur.execute("drop table if exists Apple_Prices_Daily")
cur.execute("drop table if exists Apple_Products")
#add the products and their IDs into the products table
cur.execute(prods)
cur.execute(prices)
cur.execute("INSERT INTO Apple_Products VALUES (1, 'Airpods Pro')")
cur.execute("INSERT INTO Apple_Products VALUES (2, 'Airpods 3rd Gen')")
cur.execute("INSERT INTO Apple_Products VALUES (3,'Airpods Max Silver')")
cur.execute("INSERT INTO Apple_Products VALUES (4,'Airpods Max Space Grey')")
cur.execute("INSERT INTO Apple_Products VALUES (5,'Airpods Max Sky Blue')")
cur.execute("INSERT INTO Apple_Products VALUES (6,'Airpods Max Green')")
cur.execute("INSERT INTO Apple_Products VALUES (7,'Airpods Max Pink')")


con.commit()

con.close()
