# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 09:43:31 2021

@author: AshleyHowarth
"""

#adding data
import sqlite3
con = sqlite3.connect('Airpods.db')
cur = con.cursor()

#edit date for days in the past
#make up prices
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "2021-12-18", 239.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "2021-12-18", 149.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "2021-12-18", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "2021-12-18", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "2021-12-18", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "2021-12-18", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "2021-12-18", 549.0)')

cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "2021-12-17", 239.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "2021-12-17", 149.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "2021-12-17", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "2021-12-17", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "2021-12-17", 529.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "2021-12-17", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "2021-12-17", 549.0)')

cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "2021-12-16", 229.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "2021-12-16", 149.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "2021-12-16", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "2021-12-16", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "2021-12-16", 529.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "2021-12-16", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "2021-12-16", 549.0)')

cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "2021-12-15", 229.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "2021-12-15", 159.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "2021-12-15", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "2021-12-15", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "2021-12-15", 529.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "2021-12-15", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "2021-12-15", 549.0)')

cur.execute('INSERT INTO Apple_Prices_Daily VALUES (1, "2021-12-14", 229.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (2, "2021-12-14", 159.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (3, "2021-12-14", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (4, "2021-12-14", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (5, "2021-12-14", 529.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (6, "2021-12-14", 549.0)')
cur.execute('INSERT INTO Apple_Prices_Daily VALUES (7, "2021-12-14", 549.0)')

con.commit()

con.close()